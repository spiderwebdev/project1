<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <meta content="width=device-width, initial-scale=1" name="viewport">
                    <meta content="" name="description">
                        <meta content="" name="author">
                        <link href="<?php echo base_url();?>/asset//img/favicon/favicon.ico" rel="icon">
                                <title>
                                    Institution List
                                </title>
                                <!-- Bootstrap Core CSS -->
                                <link href="<?php echo base_url();?>/asse/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
                                    <!-- Menu CSS -->
                                    <link href="<?php echo base_url();?>/asse/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
                                        <!-- Menu CSS -->
                                        <link href="<?php echo base_url();?>/asse/bower_components/morrisjs/morris.css" rel="stylesheet">
                                            <!-- Custom CSS -->
                                            <link href="<?php echo base_url();?>/asse/css/style.css" rel="stylesheet">
                                                <link href="<?php echo base_url();?>/asse/css/w3.css" rel="stylesheet">
                                                    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                                                    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                                                    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
                                                </link>
                                            </link>
                                        </link>
                                    </link>
                                </link>
                            </link>
                        </meta>
                    </meta>
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel">
            </div>
        </div>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-toggle hidden-sm hidden-md hidden-lg " data-target=".navbar-collapse" data-toggle="collapse" href="javascript:void(0)">
                        <i class="ti-menu">
                        </i>
                    </a>
                    <div class="top-left-part">
                        <a class="logo" href="<?php echo base_url();?>/index.php/home/admin">
                            <i class="glyphicon glyphicon-fire">
                            </i>
                            <span class="hidden-xs">
                                PYFS <span style="font-size: 10px;">Admin</span>
                            </span>
                        </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li>
                            <a class="open-close hidden-xs hidden-lg waves-effect waves-light" href="javascript:void(0)">
                                <i class="ti-arrow-circle-left ti-menu">
                                </i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li>
                            <a class="profile-pic" href="#">
                                <img alt="user-img" class="img-circle" src="<?php echo base_url();?>/asse/images/users/hritik.jpg" width="36">
                                    <b class="hidden-xs">
                                        Hi Admin
                                    </b>
                                </img>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <div class="navbar-default sidebar nicescroll" role="navigation">
                <div class="sidebar-nav navbar-collapse ">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <div class="input-group custom-search-form">
                                <input class="form-control" placeholder="Search..." type="text">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="ti-search">
                                            </i>
                                        </button>
                                    </span>
                                </input>
                            </div>
                        </li>
                        <li>
                            <a class="waves-effect" href="<?php echo base_url();?>/index.php/home/admin">
                                <i class="ti-layout fa-fw">
                                </i>
                                Institution List
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect" href="<?php echo base_url();?>/index.php/home/back_recommendations">
                                <i class="ti-face-smile fa-fw">
                                </i>
                                Recommendation
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect" href="<?php echo base_url();?>/index.php/home/back_scholarships">
                                <i class="ti-location-pin fa-fw">
                                </i>
                                Scholarship Links
                            </a>
                        </li>
                    </ul>
                    <div class="center p-20">
                        <span class="hide-menu">
                            <a class="btn btn-info btn-block btn-rounded waves-effect waves-light" href="<?php echo base_url();?>/index.php/home/logout" target="_blank">
                                Log Out
                            </a>
                        </span>
                    </div>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-12">
                            <h4 class="page-title">
                                Scholarship Links
                            </h4>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="<?php echo base_url();?>/index.php/home/admin">
                                        Dashboard
                                    </a>
                                </li>
                                <li class="active">
                                    Scholarship Links
                                </li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                        <form action = "<?php echo base_url();?>/index.php/inputVerifier/add_sch" class="w3-container" method="POST">
                            <div class="w3-row-padding">
                                <div class="w3-quarter">
                                    <input class="w3-input w3-border" name = "name" placeholder="Scholarship Name" required="" type="text">
                                    </input>
                                </div>
                                <div class="w3-quarter">
                                    <input class="w3-input w3-border" name = "website" placeholder="Website" required="" type="url">
                                    </input>
                                </div>
                                <div class="w3-quarter">
                                    <input class="w3-input w3-border" name="deadline" placeholder="Deadline" required="" type="text">
                                    </input>
                                </div>
                                <div class="w3-quarter">
                                    <input class="w3-input w3-border" name="place" placeholder="Place" required="" type="text">
                                    </input>
                                </div>
                                <p>
                                    <input class="w3-btn w3-blue" type="submit" value="Add Scholarship Link" style="margin-left: 9px; margin-top: 15px;">
                                    
                                </p>
                            </div>
                        </form>
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="white-box">
                                    <h3>
                                        Scholarship List
                                    </h3>
                                    <!-- <p class="text-muted">Add class <code>.table</code></p> -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Scholarship Name
                                                    </th>
                                                    <th>
                                                        Website
                                                    </th>
                                                    <th>
                                                        Deadline
                                                    </th>
                                                    <th>
                                                        Place
                                                    </th>
                                                    <th>
                                                        Edit
                                                    </th>
                                                    <th>
                                                        Delete
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                  for($x = 0; $x < count($table_values); $x++){
                                                    echo $table_values[$x];
                                                  }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /#page-wrapper -->
                <footer class="footer text-center">
                    2018 © Plan Your Future Studies.
                </footer>
            </div>
            <!-- /#wrapper -->
            <!-- jQuery -->
            <script src="<?php echo base_url();?>/asse/bower_components/jquery/dist/jquery.min.js">
            </script>
            <!-- Bootstrap Core JavaScript -->
            <script src="<?php echo base_url();?>/asse/bower_components/bootstrap/dist/js/bootstrap.min.js">
            </script>
            <!-- Menu Plugin JavaScript -->
            <script src="<?php echo base_url();?>/asse/bower_components/metisMenu/dist/metisMenu.min.js">
            </script>
            <!--Nice scroll JavaScript -->
            <script src="<?php echo base_url();?>/asse/js/jquery.nicescroll.js">
            </script>
            <!--Wave Effects -->
            <script src="<?php echo base_url();?>/asse/js/waves.js">
            </script>
            <!-- Custom Theme JavaScript -->
            <script src="<?php echo base_url();?>/asse/js/myadmin.js">
            </script>
        </div>
    </body>
</html>
