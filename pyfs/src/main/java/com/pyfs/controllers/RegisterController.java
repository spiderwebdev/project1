package com.pyfs.controllers;

import javax.validation.Valid;

import com.pyfs.entities.User;
import com.pyfs.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegisterController {
	@Autowired
	private UserService userService;
	
	@GetMapping("/admin")
	public String registerForm(Model model) {

		model.addAttribute("user", new User());
		return "admin/admin";
	}
	
	
	@PostMapping("/admin")
    public String registerUser(@Valid User user, BindingResult bindingResult, Model model) {
		if(bindingResult.hasErrors()) {
			return "admin/admin";
		}
		userService.createUser(user);
		
		return "admin/admin";

	}

}
