package com.pyfs.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	  
	@RequestMapping(value = {"/", "/home", "/index"})
	public String showIndexPage() {
		return "index";  
	}

	@GetMapping("/field_of_studies")
	public String showFOS() {
		return "field_of_studies";  
	}

	@GetMapping("/institutions")
	public String showIns() {
		return "institutions";  
	}
	
	@GetMapping("/about")
	public String about() {
		return "about";  
	}
	
	@GetMapping("/recommendations")
	public String recommendations() {
		return "recommendations";  
	}
	
	@GetMapping("/scholarships")
	public String scholarships() {
		return "scholarships";  
	}
	
	@GetMapping("/edit_institutions")
	public String edit_insitutions() {
		return "admin/edit_institutions";  
	}

	@GetMapping("/back_institutions")
	public String back_insitutions() {
		return "institutions/back_institutions";  
	}
	
	@GetMapping("/back_recommendations")
	public String back_recommendations() {
		return "admin/back_recommendations";  
	}

	@GetMapping("/back_scholarships")
	public String back_scholarships() {
		return "admin/back_scholarships";  
	}
	
	@GetMapping("/login") 
	public String showLoginForm() {
		
		return "/login";  
	}
	
	
	  
	

}
