package com.pyfs;

import com.pyfs.entities.User;
import com.pyfs.services.UserService;
import com.sun.glass.ui.Application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.pyfs.repositories")
public class PyfsApplication extends SpringBootServletInitializer implements  CommandLineRunner {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {    
		return application.sources(Application.class);
		}
	
	@Autowired
	   private UserService userService;
	     
	public static void main(String[] args) {
		SpringApplication.run(PyfsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		  {
    		  User newAdmin = new User("admin@mail.com", "Admin", "admin");
    		  userService.createAdmin(newAdmin); 
    	  }
	}
}
