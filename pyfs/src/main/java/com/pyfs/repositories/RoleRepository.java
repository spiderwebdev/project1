package com.pyfs.repositories;

import com.pyfs.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository  extends JpaRepository<Role, String>{
 
}
